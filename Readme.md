# Training Data for Correlation Power Analysis

The EM radiation measures for the last AES round SBoxes are in the "data.bin" file.
The file format is:
- [Int32,1] trace_len: length of one traces (all traces have the same length)
- [Int32,1] traces_count: traces count (how many traces in the file)
- [Int8,trace_len] trace[0]: first trace,
- ...
- [Int8,trace_len] trace[traces_count-1]: last trace

The plaintexts are store as hexstrings in "texts.bin"
